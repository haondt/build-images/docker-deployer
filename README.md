# docker-deployer

docker image for running docker host deployment

# usage

## scripts

in the docker image, the `scripts` directory is mounted at `scripts`.

`/scripts/get_changes.py` - prints a list of changed services

example usage:

```shell
python3 /scripts/get_changes.py > changed_services.txt
```

`/scripts/build.py` - builds, tars and encrypts docker compose bundle

example usage:

```shell
cat changed_services.txt | python3 /scripts/build.py $ENCRYPTION_KEY 
python3 /scripts/build.py $ENCRYPTION_KEY service1 service2 service3
```

`/scripts/untar.py` - decrypts and untars docker compose bundle

example usage:
```shell
python3 /scripts/untar.py $ENCRYPTION_KEY output_dir
```

`/scripts/get_containers.py`  - list the containers listed the the docker compose of a changed service. these are the containers that need to be recreated.

example usage:
```shell
python3 /scripts/get_containers.py service1 service2 service3
cat changed_services.txt | python3 /scripts/get_containers.py
```

## notes
- encryption key should be a 32 byte hex-encoded secret
- `build.py` will tar the docker bundle, and then encrypt that in a file called `build.enc`
- equivalently, `untar.py` will look for a file called `build.enc` to decrypt + untar into the output directory
